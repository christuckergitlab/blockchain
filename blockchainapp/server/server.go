package server

import (
	"blockchainapp/blockchain"
	"blockchainapp/database"
	"embed"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

//go:embed public
var staticFiles embed.FS
var blockChainStore *database.BlockChainStore

type postRequestBody struct {
	Value string `json:"value"`
}

type postResponseBody blockchain.BlockChain

func (prb *postResponseBody) write(writer io.Writer) (err error) {
	enc := json.NewEncoder(writer)
	err = enc.Encode(prb)
	return
}

func parsePostRequestBody(reader io.Reader) (result postRequestBody, err error) {
	dec := json.NewDecoder(reader)
	dec.DisallowUnknownFields()
	err = dec.Decode(&result)
	return
}

func handleGet(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("handleGet firing")
	// bc, err := parseReqBody(req)
	// if err != nil {
	// 	fmt.Printf("error attempting to parse json body: %v", err)
	// 	http.Error(res, "invalid request", http.StatusBadRequest)
	// }
	res.Header().Set("Content-Type", "application/json")
	return
}

// handles new block chains
// initializes blockchain with first value (from req), id, and hash
func handlePost(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("handlePost firing \n")
	// should be { Value: [some value] }
	params, err := parsePostRequestBody(req.Body)
	if err != nil {
		fmt.Printf("error attempting to parse json body: %v", err)
		http.Error(res, "invalid request", http.StatusBadRequest)
		return
	}
	bc, err := blockchain.InitBlockChain(params.Value)
	fmt.Printf("new bc in handlePost: %v", bc)
	// add to db
	// err = database.SaveNewBlockChain(bc)
	// if err != nil {
	// 	fmt.Printf("error attempting to add bc to database: %v", err)
	// 	http.Error(res, "internal server error", http.StatusInternalServerError)
	// 	return
	// }
	res.Header().Set("Content-Type", "application/json")
	prb := postResponseBody(bc)
	err = prb.write(res)
	if err != nil {
		fmt.Printf("error attempting to serialize json: %v", err)
		http.Error(res, "internal server error", http.StatusInternalServerError)
		return
	}
	return
}

func handlePut(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("handlePut firing")
}

func handleDelete(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("handleDelete firing")
}

func requestRouter(res http.ResponseWriter, req *http.Request) {
	fmt.Printf("requestHandler getting hit with %v request \n", req.Method)
	switch req.Method {
	case "GET":
		handleGet(res, req)
	case "POST":
		handlePost(res, req)
	case "PUT":
		handlePut(res, req)
	case "DELETE":
		handleDelete(res, req)
	}
}

// Serve - serves
func Serve(port int, bcStore *database.BlockChainStore) {
	r := mux.NewRouter()
	p := fmt.Sprintf(":%d", port)
	publicDir, err := fs.Sub(staticFiles, "public")
	if err != nil {
		log.Fatal("static filesssss not found")
	}
	blockChainStore = bcStore
	r.PathPrefix("/").Handler(http.FileServer(http.FS(publicDir)))
	r.HandleFunc("/blockchains", requestRouter).Methods("GET", "POST", "PUT", "DELETE")
	fmt.Printf("blockchain server running on port %v\n", port)
	log.Fatal(http.ListenAndServe(p, r))
}
