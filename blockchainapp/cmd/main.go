package main

import (
	"blockchainapp/blockchainapp/server"
	"blockchainapp/database"
	"fmt"
	"os"
)

// fileExists - takes a filepath (string) and returns true if path is a file and also not a directory
func fileExists(filepath string) bool {
	info, err := os.Stat(filepath)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func main() {
	fmt.Printf("starting blockchain app \n")
	db, err := database.OpenDatabase("./db.sqlite")
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open db.. err: %v\n", err)
		os.Exit(1)
	}
	bcStore, err := database.NewBlockChainStore(db)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to create new SessionStore.. err: %v\n", err)
		os.Exit(1)
	}
	// init schema is db does not exist
	newDb := !fileExists("./db.sqlite")
	if newDb {
		schemaError := bcStore.InitSchema()
		if schemaError != nil {
			fmt.Fprintf(os.Stderr, "failed to initialize schema.. err: %v\n", schemaError)
			os.Exit(1)
		}
	}

	// run server
	server.Serve(3000, bcStore)
}
