package database

import (
	"blockchainapp/blockchain"
	"database/sql"
	"fmt"

	// need this _ to register drivers
	_ "github.com/mattn/go-sqlite3"
)

//	schema:
//		table: chains
//			columns: chainId, chainName
//		table: blocks
//			columns: blockId, chainId (foreign key), timestamp, value, hash

type BlockChainStore struct {
	blockchainDB *sql.DB
}

// OpenDatabase - takes a path to a sqlite db, opens db, returns pointer to db
func OpenDatabase(path string) (db *sql.DB, err error) {
	// fmt.Printf("path to db: %v\n", path)
	return sql.Open("sqlite3", path)
}

// NewBlockChainStore - instantiates a new BlockChainStore with a pointer to a sql.DB
func NewBlockChainStore(db *sql.DB) (result *BlockChainStore, err error) {
	bcs := BlockChainStore{
		blockchainDB: db,
	}
	result = &bcs
	return
}

func (bcs *BlockChainStore) InitSchema() (err error) {
	// design schema
	return
}

// InsertChain - BlockChainStore method that adds a new block chain to the database
func (bcs *BlockChainStore) InsertChain(bc blockchain.BlockChain) (err error) {
	db := bcs.blockchainDB
	var stmt *sql.Stmt
	stmt, err = db.Prepare(`
		INSERT INTO chains (chainId, name) VALUES (?, ?);
	`)
	if err != nil {
		return
	}
	_, err = stmt.Exec(bc.Id, bc.Name)
	if err != nil {
		fmt.Printf("unable to execute blockchain db insert: %v\n", err)
		return
	}
	return
}

func (bcs *BlockChainStore) InsertBlock(b blockchain.Block) (err error) {
	db := bcs.blockchainDB
	var stmt *sql.Stmt
	stmt, err = db.Prepare(`
		INSERT INTO blocks (blockId, chainId, timestamp, value, hash) VALUES (?, ?, ?, ?, ?);
	`)
	_, err = stmt.Exec(b.Id, b.ChainId, b.Timestamp, b.Value, b.Hash)
	if err != nil {
		fmt.Printf("unable to execute blockchain db insert: %v\n", err)
		return
	}
	return
}

// FIXME: GetBlockTimestamp - used for security check. Before adding a block to a chain, verify that the latest block has a timestamp that is not AFTER the current one being added (see blockchain.VerifyNewBlock)
func (bcs *BlockChainStore) GetBlockTimestamp(blockId string) (result string, err error) {
	// db := bcs.blockchainDB
	// var stmt *sql.Stmt
	// stmt, err = db.Prepare(`
	// 	SELECT FROM blocks WHERE id
	// `)
	return
}
