const express = require('express');

const app = express();
const PORT = 3000;
app.use(express.json());
app.use(express.static(__dirname + '/../api/server/public'));

app.post('/blockchains', (req, res) => {
  console.log('/blockchains POST hit');
  res.status(200).send();
});

app.listen(PORT, () => {
  console.log('express listening on port ' + PORT)
});
