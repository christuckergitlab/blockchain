# blockChain
## overview:
A simple Golang implementation of a block chain where each block holds: <br>
1. a string value
2. a sha256 hash
## structs:
```
type BlockChain struct {
	path  string
	chain []*block
}

type block struct {
	value string
	hash  []byte
}
```

## functions:
- **InitBlockChain(initialVal string) BlockChain** - Initalizes blockchain with it's first block. The value parameter of the first block is the given string. The hash parameter of the first block is the checksum of the of the first string.


## type BlockChain methods:
- **AddBlock(val string)** - Adds a block to the blockchain. Takes a string which will be the value of the new block. The hash for the new block is calculated by taking the checksum of the previous block, appending the byte array of the new value (val) to it, and getting the checksum of the result.
-  **VerifyChain() bool** - Iterates through all blocks verifying the checksum relationship to the previous block.

## web app features (in development):
- domain: TBD
- view source code
- create new block chain
- view existing block chains
- download existing block chain data
- add block to existing block chain

## resources:
- [package crypto/sha256 docs](https://pkg.go.dev/crypto/sha256@go1.16.2)
- [package bytes docs](https://pkg.go.dev/bytes)