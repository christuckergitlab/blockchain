package blockchain

import (
	"bytes"
	"crypto/sha256"
	"os"

	"github.com/google/uuid"
	"github.com/spf13/afero"
)

var appFs = afero.NewOsFs()

type BlockChain struct {
	Id    string   `json:"id"`
	Name  string   `json:"name"`
	Chain []*Block `json:"chain"`
}

type Block struct {
	Id        string `json:"id"`
	ChainId   string `json:"chainId"`
	Timestamp string `json:"timestamp"`
	Value     string `json:"value"`
	Hash      []byte `json:"hash"`
}

// FIXME: VerifyNewBlock - Verify that the block being added does not have a timestamp that is less than the last block in the chain
func VerifyNewBlock(blockId string) bool {
	return false
}

// AddBlock - adds a block to the blockchain. Takes a string which will be the value of the new block. The hash for the new block is calculated by taking the checksum of the previous block, appending the byte array of the new value to it, and getting the checksum of the result.
func (bc *BlockChain) AddBlock(val string) (err error) {
	newBlock := new(Block)
	newBlock.Value = val
	previousSum := bc.Chain[len(bc.Chain)-1].Hash
	newBytes := []byte(val)
	hash := sha256.New()
	hash.Write(previousSum)
	newBlock.Hash = hash.Sum(newBytes)
	bc.Chain = append(bc.Chain, newBlock)
	return
}

// Exists reports whether the named file or directory exists.
func Exists(name string) bool {
	if _, err := appFs.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// InitBlockChain - initalizes blockchain with it's first block. The value parameter of the first block is the given string. The hash parameter of the first block is the checksum of the byte array of the first string.
func InitBlockChain(initialVal string) (bc BlockChain, err error) {
	firstBlock := new(Block)
	firstBlock.Value = initialVal
	hash := sha256.New()
	valBytes := []byte(initialVal)
	firstBlock.Hash = hash.Sum(valBytes)
	bc.Chain = append(bc.Chain, firstBlock)
	bc.Id = uuid.New().String()
	return
}

// VerifyChain - iterates through all blocks verifying the relationship to the previous block.
func (bc *BlockChain) VerifyChain() bool {
	for i := 1; i < len(bc.Chain); i++ {
		previousHash := bc.Chain[i-1].Hash
		currentValue := bc.Chain[i].Value
		valBytes := []byte(currentValue)
		hash := sha256.New()
		hash.Write(previousHash)
		checkSum := hash.Sum(valBytes)
		sameBytes := bytes.Compare(checkSum, bc.Chain[i].Hash)
		if sameBytes != 0 {
			return false
		}
	}
	return true
}

// func WriteToFile(path string, data string) (err error) {
// 	// If the file doesn't exist, create it, or append to the file
// 	f, err := appFs.OpenFile(path, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	if _, err := f.Write([]byte(data + "\n")); err != nil {
// 		f.Close() // ignore error; Write error takes precedence
// 		log.Fatal(err)
// 	}
// 	if err := f.Close(); err != nil {
// 		log.Fatal(err)
// 	}
// 	return
// }

// func main() {
// 	tuckerChain, err := InitBlockChain("tucker", "./temp/")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	fmt.Printf("tuckerChain: %v\n", tuckerChain)
// 	fmt.Printf("tuckerChain.path: %v\n", tuckerChain.path)
// 	fmt.Printf("tuckerChain.chain: %v\n", tuckerChain.chain)
// }
