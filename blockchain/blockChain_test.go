package blockchain

import (
	"testing"

	"github.com/spf13/afero"
)

func TestBlockChain(t *testing.T) {
	appFs = afero.NewMemMapFs()
	bc, err := InitBlockChain("first-test-block")
	if err != nil {
		t.Errorf("failed to initalize blockchain: %v\n", err)
	}
	if len(bc.Chain) != 1 {
		t.Errorf("failed to initialize blockchain with first block")
	}
	// firstBlock := bc.Chain[len(bc.Chain)-1]
	err = bc.AddBlock("second-test-block")
	if err != nil {
		t.Errorf("failed to add second block")
	}
	bc.AddBlock("third-test-block")
	if err != nil {
		t.Errorf("failed to add third block")
	}
	if len(bc.Chain) != 3 {
		t.Errorf("failed to add blocks to blockchain")
	}
	if bc.Chain[len(bc.Chain)-1].Value != "third-test-block" {
		t.Errorf("incorrect value for most recent block")
	}
	valid_1 := bc.VerifyChain()
	if valid_1 != true {
		t.Errorf("failed to correctly validate blockchain")
	}
	// fail case
	bc.Chain[len(bc.Chain)-1].Value = "test-corruption"
	valid_2 := bc.VerifyChain()
	if valid_2 != false {
		t.Errorf("failed to recognize invalid blockchain")
	}
}
