import React from 'react';
import axios from 'axios';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      initialValText: ""
    }
  };

  componentDidMount() {
    console.log("mounted");
  };

  // fixme: security measures for inputs (also must implement security measures in server code)
  handleCreateBlockchain() {
    const { initialValText } = this.state;
    axios.post('/blockchains', {
      value: initialValText
    })
    .then((res) => {
      console.log('successfully added new blockchain. res: ', res);
      this.setState({ initialValText: '' });
    })
    .catch((err) => {
      console.log('err: ', err);
      this.setState({ initialValText: '' });
    });
  };

  handleInitialValTextChange(val) {
    this.setState({ initialValText: val });
  };

  render() {
    const { initialValText } = this.state;
    return (
        <div className="AppComponentDiv">
            <a href="https://gitlab.com/christuckergitlab/blockchain">
                <div>source</div>
            </a>
            <br></br>
            <div className="newBlockChainDiv">
              <div>
                <input
                  type="text"
                  placeholder="first block value"
                  onChange={(e) => { this.handleInitialValTextChange(e.target.value); }}></input>
              </div>
              <button
              onClick={() => { this.handleCreateBlockchain(); }}>
                create new blockchain
              </button>
            </div>
            <div>
                {/* view existing blockchains */}
            </div>
        </div>
    );
  };
};

export default App;